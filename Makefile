INSTALL_PATH?=`(test -e /opt/local/share/games && echo /opt/local/share/games/) || echo /usr/share/games/fortunes/`

herzog.dat: herzog
	strfile -c % $< $@

.PHONY: install
install: herzog herzog.dat
	cp $^ $(INSTALL_PATH)

.PHONY: uninstall
uninstall:
	-rm $(INSTALL_PATH)herzog $(INSTALL_PATH)herzog.dat

.PHONY: test
test:
	@sh .profile

.PHONY: clean
clean:
	-rm herzog.dat
