A small collection of Werner Herzog quotes suitable for use with the `fortune` utility.

`# make install`

```
                             /\             /\
                            |`\\_,--="=--,_//`|
                            \ ."  :'. .':  ". /
                           ==)  _ :  '  : _  (==
                             |>/O\   _   /O\<|
                             | \-"~` _ `~"-/ |
                            >|`===. \_/ .===`|<
                      .-"-.   \==='  |  '===/   .-"-.
.--------------------{'. '`}---\,  .-'-.  ,/---{.'. '}--------------------.
 )                   `"---"`     `~-===-~`     `"---"`                   (
(  "We ought to be grateful that the universe out there knows no smile."  )
 )                                                                       (
'-------------------------------------------------------------------------
```
